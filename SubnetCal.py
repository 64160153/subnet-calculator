import ipaddress
def validate_network_class(network_class):
    return network_class in ['A', 'B', 'C']

def validate_ip_address(ip_address):
    try:
        ipaddress.ip_network(ip_address, strict=False)
        return True
    except ValueError:
        return False

def validate_subnet_mask(subnet_mask):
    try:
        ipaddress.ip_network(f'0.0.0.0/{subnet_mask}', strict=False)
        return True
    except ValueError:
        return False

def calculate_subnet(ip_address, subnet_mask):
    network = ipaddress.IPv4Network(f'{ip_address}/{subnet_mask}', strict=False)
    return network

def calculate_subnet_bits(network_class, subnet_mask):
    if network_class == 'A':
        n = 8
    elif network_class == 'B':
        n = 16
    elif network_class == 'C':
        n = 24
    mask_bits = sum(bin(int(octet)).count('1') for octet in subnet_mask.split('.'))
    subnet_bits = mask_bits - n
    return subnet_bits, mask_bits

def calculate_octet_range(network_class):
    if network_class == 'A':
        return "1 - 126"
    elif network_class == 'B':
        return "128 - 191"
    elif network_class == 'C':
        return "192 - 223"
    else:
        return "Invalid"

def convert_to_hex(ip_address):
    return '.'.join(format(int(octet), '02X') for octet in ip_address.split('.'))

def calculate_subnet_bitmap(network_class, subnet_bits):
    leading_bits = ""
    if network_class == 'A':
        leading_bits = "0"
        networks = 7
        hosts = 24
    elif network_class == 'B':
        leading_bits = "10"
        networks = 14
        hosts = 16
    elif network_class == 'C':
        leading_bits = "110"
        networks = 21
        hosts = 8
        
    hosts = hosts - subnet_bits
    
    bitmap = leading_bits + "n"*networks + "s"*subnet_bits + "h"*hosts
    parts = [
        bitmap[:8],
        bitmap[8:16],
        bitmap[16:24],
        bitmap[24:32]
    ]
    
    return '.'.join(parts)
    
def main():
    while True:
        network_class = input("Enter Network Class (A, B, or C): ").upper()
        is_valid = validate_network_class(network_class)
        if is_valid:
            break
        else:
            print(f"Invalid Network Class. Please enter A, B, or C")
            
    while True:
        ip_address = input("Enter IP address: ")
        if validate_ip_address(ip_address):
            break
        else:
            print("Invalid IP address. Please enter IP address again.")
            
    while True:
        subnet_mask = input("Enter Subnet Mask: ")
        if validate_subnet_mask(subnet_mask):
            break
        else:
            print("Invalid Subnet Mask. Please enter Subnet Mask again.")
            
    subnet = calculate_subnet(ip_address, subnet_mask)
    subnet_bits, mask_bits = calculate_subnet_bits(network_class, subnet_mask)
    octet_range = calculate_octet_range(network_class)
    subnet_bitmap = calculate_subnet_bitmap(network_class, subnet_bits)
    hex_ip = convert_to_hex(ip_address)
    
    print("\nSubnet Calculator")
    print(f"Network Class: {network_class}")
    print(f"First Octet Range: {octet_range}")
    print(f"IP Address: {ip_address}")
    print(f"Hex IP Address: {hex_ip}")
    print(f"Subnet Mask: {subnet.netmask}")
    print(f"Wildcard Mask: {subnet.hostmask}")
    print(f"Subnet Bits: {subnet_bits}")
    print(f"Mask Bits: {mask_bits}")
    print(f"Maximum Subnets: {2 ** subnet_bits}")
    print(f"Hosts per Subnet: {subnet.num_addresses - 2}")
    print(f"Host Address Range: {subnet.network_address + 1} - {subnet.broadcast_address - 1}")
    print(f"Subnet ID: {subnet.network_address}")
    print(f"Broadcast Address: {subnet.broadcast_address}")
    print(f"Subnet Bitmap: {subnet_bitmap}")
    
if __name__ == "__main__":
    main()
